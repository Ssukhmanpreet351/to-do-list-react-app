import './App.css'
import React, { useState } from 'react'

function App() {

    const [inputTextField, setInputTextField] = useState('')
    const [todo, setTodo] = useState([])
    const [editToggle, setEditToggle] = useState(true)
    const [editItemID, setEditItemID] = useState(null)

    const inputText = (e) => {
        setInputTextField(e.target.value)
    }

    const submitTodo = (e) => {
        e.preventDefault()
        if (!inputTextField) {
            alert("Please insert text")
        } else if (inputTextField && !editToggle) {
            setTodo(
                todo.map((ele) => {
                    if (ele.id === editItemID) {
                        return { ...ele, text: inputTextField }
                    }
                    return ele;
                })
            )
            setEditToggle(true)
            setInputTextField('')
            setEditItemID(null)
        } else {
            setTodo([...todo, { text: inputTextField, id: Math.random() * 1000 }])
            setInputTextField('')
        }
    }


    const editTodo = (id) => {
        document.getElementById("input-task-field").focus();
        let newInputField = todo.find((ele) => {
            return (
                ele.id === id
            )
        })
        setEditToggle(false)
        setInputTextField(newInputField.text)
        setEditItemID(id)
    }

    const deleteTodo = (id) => {
        setTodo(todo.filter((ele) => ele.id !== id))
    }

    const deleteAllTodos = () => {
        setTodo(todo.filter((ele) => console.log("Deleted todo")))
    }

    return (
        <div>
            <header>
                <h1>To Do Task List</h1>
            </header>

            <main>
                <h4>Please input your task below:</h4>

                <form id="new-form">
                    <input
                        id="input-task-field"
                        value={inputTextField}
                        type="text"
                        placeholder="Click to enter your task."
                        onChange={inputText}
                    />
                    {
                        editToggle ?
                            <input
                                id="submit-bttn"
                                type="submit"
                                value="Add task to the list"
                                onClick={submitTodo}
                            /> :
                            <input
                                id="submit-bttn"
                                type="submit"
                                value="Update task"
                                onClick={submitTodo}
                            />
                    }
                    <input
                        id="delete-all-bttn"
                        type="button"
                        value="Delete all tasks"
                        onClick={deleteAllTodos}
                    />
                </form>

                <h2>Task List:</h2>
                {
                    todo.map((item) => {
                        return (
                            <div className="indvl-task" key={item.id}>
                                <div className="task-content">
                                    <input
                                        className="task-text"
                                        type="text"
                                        value={item.text}
                                        readOnly
                                    />
                                </div>
                                <div className="indvl-task-actions">
                                    <button className="task-edit-button" onClick={() => editTodo(item.id)}>
                                        Edit
                                    </button>
                                    <button className="task-delete-button" onClick={() => deleteTodo(item.id)}>
                                        Delete
                                    </button>
                                </div>
                            </div>
                        )
                    })
                }
            </main>
        </div>
    )
}

export default App



// const Form = () => {
    //     return (
    //         <>
    //             <form id="new-form">
    //                 <input
    //                     id="input-task-field"
    //                     value={inputTextField}
    //                     type="text"
    //                     placeholder="Click to enter your task."
    //                     onChange={inputText}
    //                 />

    //                 <input
    //                     id="submit-bttn"
    //                     type="submit"
    //                     value="Add task to the list"
    //                     onClick={submitTodo}
    //                 />

    //                 <input
    //                     id="delete-all-bttn"
    //                     type="button"
    //                     value="Delete all tasks"
    //                 />
    //             </form>
    //         </>
    //     );
    // }
    // const List = (todo) => {
    //     console.log('Inside List = ', todo)
    //     return (
    //         <>
    //             <h2>Task List:</h2>
    //             {
    //                 todo.map((item, index) => {
    //                     return <p key={index}>TASKS</p>
    //                 })
    //             }
    //         </>
    //     )
    // }
    // const ToDo = (val, ky) => {
    //     return (
    //         <>
    //             <div id="list-all-tasks" key={ky}>
    //                 <div className="indvl-task">
    //                     <div className="task-content">
    //                         <input
    //                             className="task-text"
    //                             type="text"
    //                             value={val}
    //                             readOnly
    //                         />
    //                     </div>
    //                     <div className="indvl-task-actions">
    //                         <button className="task-edit-button">
    //                             EDIT
    //                         </button>

    //                         <button className="task-delete-button">
    //                             DELETE
    //                         </button>
    //                     </div>
    //                 </div>
    //             </div>
    //         </>
    //     )
    // }

